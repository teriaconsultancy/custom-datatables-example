/*! 
 * 
 * 
 *
 * @version     1.0.0
 * @author      Nishant Teria
 * @copyright   (c) Terias
 * @license     MIT
 */
(function( factory ){
    /* eslint-disable */
       if ( typeof define === 'function' && define.amd ) {
          // AMD
          define( ['jquery', 'datatables.net'], function ( $ ) {
             return factory( $, window, document );
          } );
       }
       else if ( typeof exports === 'object' ) {
          // CommonJS
          module.exports = function (root, $) {
             if ( ! root ) {
                root = window;
             }
    
             if ( ! $ || ! $.fn.dataTable ) {
                $ = require('datatables.net')(root, $).$;
             }
    
             return factory( $, root, root.document );
          };
       }
       else {
          // Browser
          factory( jQuery, window, document );
       }
    /* eslint-enable */
    }(function( $, window, document ) {
        'use strict';
        var $1;
        $1 = $;
        var dataTable = $1.fn.dataTable;
        var DataTable$ = $1.fn.DataTable;
       /*
       * API
       */
       /*
       * API
       */
       //var Api = $1.fn.dataTable.Api;

        class ColumnFilter {
            constructor(settings, opts) {
                var that = this;
                that.initOptions = $1.extend(true, {}, ColumnFilter.defaults, opts);
                var apiDataTable$ = new DataTable$.Api(settings);
                var settingsArray = apiDataTable$.settings();
                var settingsObject = settingsArray[0];
                // Get settings object
                that.instanceValues = {
                    dtSettings: settingsObject,
                    api: apiDataTable$,
                    searchFunction: null,
                    searchedOperatorType: null,
                    searchedValue: null,
                    searchedColIndex: null
                };

                // Check if ColumnFilter have already been initialised on this table
                if (settingsObject._columnFilter) {
                    return;
                }
                settingsObject._columnFilter = this;
                if (!settingsObject._bInitComplete) {
                    that._constructor(apiDataTable$);
                }
                else {
                    apiDataTable$.one('init.dt.dtColumnFilter', function () {
                        that._constructor(apiDataTable$);
                    });
                }
                return that;
            }
            getNode() {
                return $1('tr' + '.' + ColumnFilter.Classes.FilterContainer.Row);
            }
            /**
             * _constructor private function
             * @description
             *   This is main function called from init, so as to create the filter and customize search
             *   so as to consider chosen operator.
             * @returns  Nothing
             */
            _constructor() {
                var self = this;
                self._createFilterContainer();
                self._customizeSearch();
            }
            /**
             * _createFilterContainer private function
             
             * @description
             *  In Thead of table, adds new row <tr> with class ColumnFilter.Classes.FilterContainer.Row
             *     after cloning from header row in THead.
             *  In this new row, for each TH column,
             *     adds filter container div element with class ColumnFilter.Classes.FilterContainer.Column        *
             * @returns Nothing
             */
            _createFilterContainer() {
                var self = this;
                var header = $(self.instanceValues.dtSettings.nTHead);
                var headerRowCloned = $1($1(header).find('tr')).clone();
                var classesFilterContainer = ColumnFilter.Classes.FilterContainer;
                var classFilterContainerRow = classesFilterContainer.Row;
                var classFilterContainerColumn = classesFilterContainer.Column;
                var classFilterControl = classesFilterContainer.Control;
                var classImage = ColumnFilter.Classes.Common.Image;
                var classGridMenuButton = classesFilterContainer.GridMenuButton;
                var classesFilterImage = classImage + ' ' + classesFilterContainer.FilterImage;
                var classesArrowDownImage = classImage + ' ' + classesFilterContainer.ArrowDownImage;
                var classesGridMenuButton = classesFilterContainer.MenuButton + ' ' + classGridMenuButton;
                var classesInput = classesFilterContainer.Input;
                var attrNameOperator = ColumnFilter.DataAttributes.Operator;
                var sourceImages = ColumnFilter.ImageSources;
                var prefix = ColumnFilter.ImageSrcPrefix;
                var srcArrowDownImage = prefix + sourceImages.ArrowDownImage;
                var isCheckBoxesRendered = self.instanceValues.api.checkboxes !== undefined;
                console.log(isCheckBoxesRendered);
                headerRowCloned.find('th').each(function (colIdx) {
                    $1(this)
                        .removeClass()
                        .removeAttr('aria-controls')
                        .removeAttr('aria-label')
                        .removeAttr('aria-sort')
                        .html('');
                    var allowed = isCheckBoxesRendered ? (colIdx > 0 ? true : false) : true;
                    if (allowed === true) {
                        var isDateColumn = isCheckBoxesRendered ? colIdx === 5 : colIdx === 4;
                        var colType = ColumnFilter.ColumnTypes.String; //TODO
                        var defaultOperatorConfig = self._getDefaultOperatorConfig(colType);
                        var defaultOperatorValue = defaultOperatorConfig.Value;
                        var defaultOperatorImageSrc = prefix + defaultOperatorConfig.ImageSrc;
                        var filterControlContainerColumn = $1(
                            '<div class = "' + classFilterContainerColumn + '" >' +
                            '<div class = "' + classFilterControl + '" >' +
                            '<div title = "' + defaultOperatorConfig.DisplayName + '" class = "' + classesGridMenuButton + '" >' +
                            '<img ' + attrNameOperator + ' = "' + defaultOperatorValue + '"' + ' class = "' + classesFilterImage + '" src = "' + defaultOperatorImageSrc + '" >' +
                            '<img class = "' + classesArrowDownImage + '" src = "' + srcArrowDownImage + '">' +
                            '</div>' +
                            (isDateColumn === true
                                ?
                                '<input type = "date" />'
                                :
                                '<input type = "text" class = "' + classesInput + '" maxlength = "38" >') +
                            '</div>' +
                            '</div>');
                        filterControlContainerColumn
                            .appendTo($1(this));
                        self._setEvents(filterControlContainerColumn, colType, colIdx);
                    }

                });
                headerRowCloned
                    .addClass(classFilterContainerRow)
                    .appendTo($1(header));
            }
            /**
             * _setEvents private function
             * @param
             *   self : The reference to ColumnFilter function
             *   filterControlContainerColumn: (Type - $1(element)) The reference to newly added <div class="filter-container-column" > element in TH of new row
             *   colType: (Type - Enum) String/Boolean/Number/Date etc. Used to decide what operators will show in filter menu.
             *   colIdx: (Type - Integer) ColumnIndex of the column for which events are to be binded.
             * @description Sets following events
             *  On click of filterControlContainerColumn element in TH, opens the filter menu at that location.
             *  On click of operator from filter menu that opens in above step,
             *    closes the filter menu and sets src and data-operator of control in filterControlContainerColumn element
             *  On any change in textbox of filter, sets the following values in instance of datatable-
             *      - operator chosen from filter menu
             *      - value typed by user
             *       And calls .draw() method of table which internally calls the custom search function
             * On focus in in textbox of filter, closes the filter menu.
             * @returns Nothing
             */
            _setEvents(filterControlContainerColumn, colType, colIdx) {
                var self = this;
                var api = self.instanceValues.api;
                var selectorFilterImage = 'img' + '.' + ColumnFilter.Classes.FilterContainer.FilterImage;
                var selectorRow = 'tr' + '.' + ColumnFilter.Classes.FilterMenu.Row;
                var selectorGridMenuButton = 'div' + '.' + ColumnFilter.Classes.FilterContainer.GridMenuButton;
                var attrNameOperator = ColumnFilter.DataAttributes.Operator;
                filterControlContainerColumn.find(selectorGridMenuButton)
                    .off("click.dtColumnFilter")
                    .on("click.dtColumnFilter", function (event) {
                        var filterImage = $1(this).find(selectorFilterImage);
                        var offset = filterImage.offset();
                        var filterMenu = self._getFilterMenu(colType, false)
                            .css({
                                left: offset.left + 5,
                                top: offset.top
                            });
                        filterMenu
                            .show()
                            .find(selectorRow)
                            .off("click.dtColumnFilter")
                            .on("click.dtColumnFilter", function (e) {
                                var img = $1(this).find('img');
                                var operatorValue = img.attr(attrNameOperator);
                                var operatorDisplayName = self._getMatchingOperatorConfig(operatorValue).DisplayName;
                                filterImage
                                    .attr('src', img.attr('src'))
                                    .attr(attrNameOperator, img.attr(attrNameOperator))
                                    .parent()
                                    .attr('title', operatorDisplayName);
                                filterMenu
                                    .hide();
                                self
                                    .instanceValues
                                    .searchedOperatorType = operatorValue;
                                api
                                    .draw();
                            });

                        //filterMenu.show();
                    });
                filterControlContainerColumn
                    .find('input')
                    .on('focusin.dtColumnFilter', function () {
                        self
                            ._getFilterMenu(colType, true)
                            .hide();
                    })
                    .on('keyup.dtColumnFilter change.dtColumnFilter clear.dtColumnFilter', function () {
                        self.instanceValues.searchedOperatorType = $1(this).parent().find(selectorFilterImage).attr(attrNameOperator);
                        self.instanceValues.searchedValue = this.value;
                        self.instanceValues.searchedColIndex = colIdx;

                        self
                            ._getFilterMenu(colType, true)
                            .hide();
                        api
                            .draw();
                    });
            }
            /**
             * _customizeSearch private function, called when .draw() is called on table.
             * @description Customizes default search in dataTables to search by using operators.
             * @returns Nothing
             */
            _customizeSearch() {
                var self = this;
                var operatorTypes = self._getOperators();
                if ($1.fn.dataTable.ext.search.indexOf(self.instanceValues.searchFunction) === -1) {
                    // Custom search function
                    self.instanceValues.searchFunction = function (settings, searchData, rowIndex, origData, counter) {
                        var searchedColIndex = self.instanceValues.searchedColIndex;
                        if (searchedColIndex == null || searchedColIndex == undefined) {
                            return true;
                        }
                        var returned = false;
                        var valueInColumnOfTable = searchData[searchedColIndex];
                        var searchedValue = self.instanceValues.searchedValue;
                        var searchedOperatorType = self.instanceValues.searchedOperatorType;
                        switch (searchedOperatorType) {
                            case operatorTypes.Equals: {
                                if (valueInColumnOfTable === searchedValue) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.DoesNotEqual: {
                                if (valueInColumnOfTable !== searchedValue) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.StartsWith: {
                                if (valueInColumnOfTable.indexOf(searchedValue) == 0) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.GreaterThanOrEquals: {
                                break;
                            }
                        }
                        return returned;
                    };
                    $1.fn.dataTable.ext.search.push(self.instanceValues.searchFunction);
                }
            }
            /**
             * _getFilterMenu private function
             * @param
             *  colType: (Type - Enum) String\Boolean\Number\DateTime etc
             *  hideExisting: (Type - Boolean) True\False. Default is false
             * @description
             *   Filter menu element is supposed to be present in document.
             *   When hideExisting is true, then it just finds the filter menu and hides it.
             *   When hideExisting is false (default), it just destroys previous instance of filter menu and
             *      creates fresh considering the operators corresponding to colType.
             * @returns
                Returns the filter menu element.
            */
            _getFilterMenu(colType, hideExisting = false) {
                var self = this;
                var classImage = ColumnFilter.Classes.Common.Image;
                var classLabel = ColumnFilter.Classes.Common.Label;
                var classesMenu = ColumnFilter.Classes.FilterMenu;
                var classFilterMenu = classesMenu.MenuButtonMenu;
                var classPopUpContent = classesMenu.PopUpContent;
                var classRow = classesMenu.Row;
                var selectorContainer = self.initOptions.selectorContainer;
                var selectorFilterMenu = 'div' + '.' + classFilterMenu;
                var filterMenu = $1(selectorContainer).find(selectorFilterMenu);
                if (hideExisting === true) { //used when required to hide existing menu
                    return filterMenu;
                }
                // else destroy existing and create new
                if (filterMenu.length > 0) {
                    filterMenu.remove();
                }
                var prefix = ColumnFilter.ImageSrcPrefix;
                var rawHtml = '<div class = "' + classesMenu.Main + ' ' + classFilterMenu + '" >' +
                    '<div class = "' + classPopUpContent + '" ><table><tbody>';
                var operatorConfigurations = self._getOperatorConfigs(colType);
                for (var cnt = 0; cnt < operatorConfigurations.length; cnt++) {
                    var src = prefix + operatorConfigurations[cnt].ImageSrc;
                    rawHtml +=
                        '<tr class = "' + classRow + '" >' +
                        '<td>' +
                        '<img ' + ColumnFilter.DataAttributes.Operator + ' = ' + '"' + operatorConfigurations[cnt].Value + '" src = "' + src + '" class = "' + classImage + '"' + '>' +
                        '</td>' +
                        '<td>' +
                        '<label class = "' + classLabel + '"' + ' >' + operatorConfigurations[cnt].DisplayName + '</label>' +
                        '</td>' +
                        '</tr>';
                }
                rawHtml += '</tbody></table></div></div>';
                $1(rawHtml).appendTo($1(selectorContainer));
                return $1(selectorContainer).find(selectorFilterMenu);
            }
            /**
             * _getOperatorConfigs private function
             * @param
             *  colType: (Type - Enum) Boolean/String/Date/Number etc
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (enum) like Number\String etc
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the list of operator configs for matching colType.
             * @returns
             *   Returns the list of ColumnFilter.OperatorConfigurations for the matched colType.
             */
            _getOperatorConfigs(colType) {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = [];
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (values[cnt].AppliesTo === colType) {
                        returned.push(values[cnt]);
                    }
                }
                return returned;
            }
            /**
             * _getDefaultOperatorConfig private function
             * @param
             *  colType: (Type - Enum) Boolean/String/Date/Number etc
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (enum) like Number\String etc
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the default operator config based upon colType.
             * @returns
             *   Returns the single object from ColumnFilter.OperatorConfigurations array
             *    where colType matches and IsDefault is true.
             */
            _getDefaultOperatorConfig(colType) {
                var self = this;
                var values = self._getOperatorConfigs(colType);
                var returned = null;
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (values[cnt].IsDefault === true) {
                        returned = values[cnt];
                        break;
                    }
                }
                return returned;
            }
            /**
             * _getMatchingOperatorConfig private function
             * @param
             *  value: The value field like 'Equals' or 'DoesNotEqual'
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (enum) like Number\String etc
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the matching operator config based upon Value.
             * @returns
             *     Returns the single object from ColumnFilter.OperatorConfigurations array
             *          where input value matches Value
             */
            _getMatchingOperatorConfig(value) {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = null;
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (values[cnt].Value === value) {
                        returned = values[cnt];
                        break;
                    }
                }
                return returned;
            }
            /**
             * _getOperators private function
             * @param none
             * @description
             *  Iterates the array of objects in ColumnFilter.OperatorConfigurations to form an object
             *   holding all values from property "Value" of each element in array.
             *   The returned object is used in switch case.       *
             * @returns object with following structure { Equals: 'Equals', DoesNotEqual:'DoesNotEqual'.... }
             */
            _getOperators() {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = {};
                for (var cnt = 0; cnt < values.length; cnt++) {
                    returned[values[cnt].Value] = values[cnt].Value;
                }
                return returned;
            }
        }

        ColumnFilter.defaults = {
            IsCaseSensitive: false
        };

        ColumnFilter.ColumnTypes = {
            String: 0,
            Number: 1,
            DateTime: 2,
            Date: 3,
            Boolean: 4
        };

        ColumnFilter.Classes = {
            FilterContainer: {
                Row: 'filter-container-row',
                Column : 'filter-container-column',
                Control : 'filter-control',
                InputControl : 'trigger-input-field date-input-field',
                FilterImage:'gwt-image-Operator',
                MenuButton: 'button menu-button',
                GridMenuButton: 'grid-menu-button',
                ArrowDownImage: 'arrow-down arrow-down-customized',            
                Input: 'text-input-field text-input-field-empty'
            },
            FilterMenu: {
                Main: 'menu',
                MenuButtonMenu: 'menu-button-menu',
                PopUpContent: 'popup-content',                
                Row: 'menu-item'
            },
            Common: {
                Image: 'gwt-image',
                Label: 'label'
            }
        }

        ColumnFilter.DataAttributes = {
            Operator: 'data-operator'
        };

        ColumnFilter.ImageSrcPrefix = 'data:image/png;base64,';
        
        ColumnFilter.ImageSources = {
            ArrowDownImage: 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAKElEQVR42mNgwAKMjIwKsAoaGxv/xyqIIoEsCJdAFwRLAAUL0QVBGACRih4O/Qn69wAAAABJRU5ErkJggg==',
            CalendarImage:'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAASElEQVQoU2NkQID/DAwMjEh8GBNFHFkB0RpACkEApBnGRrYIWZwRhUOqBizOxxCC24DNs+iqwX6EOWnEaQCFBrYIQw8lcMAAANQVGA1kMf1wAAAAAElFTkSuQmCC'
        };

        ColumnFilter.OperatorConfigurations = [
            {
                DisplayName: 'Equals',
                Value: 'Equals',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAKUlEQVQoU2NkIBEwkqieAaThPyma6KOBFBeB/UASoI8fSA5Wkv1AkgYAaUQGB3BV678AAAAASUVORK5CYII=',
                AppliesTo: ColumnFilter.ColumnTypes.String,
                IsDefault: true
            },
            {
                DisplayName: 'Does Not Equal',
                Value: 'DoesNotEqual',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAt0lEQVQoU33SMWqCQRAF4O8HU9iIRe6RLmJj4R28gU2aFOlsUluJKHgI72Arapl7JJAqgUSUhX9hWFa3mp15782b2W3cPs84tuUH/Ke4uUPY4AWvWGdcIlwqpJjv4zvjaoSUm2DbCqX7HzrZUtkhioxwwm+0VDoaYt8mk+o5AmpD544zzEu1coYuflrQIz7LpURCisfYYYU3fKFXWsoWIvkJH7WVxxkGOIRVVt80EhaY4h3LWz/gCm6AIgem/dGAAAAAAElFTkSuQmCC',
                AppliesTo: ColumnFilter.ColumnTypes.String,
                IsDefault: false
            },
            {
                DisplayName: 'Starts With',
                Value: 'StartsWith',
                ImageSrc:'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAVUlEQVQoU6WRAQoAIQgEx17ex48L4xRPipCESMOdtRKKIcV+rgUd0GXRgCfUkh2s2fYXfArNZxGJ8UpR5OcnwaR+YHdYUTNdwVtBHvHncv2sx38sOwx2MxAI0jEbYAAAAABJRU5ErkJggg==',
                AppliesTo: ColumnFilter.ColumnTypes.String,
                IsDefault: false
            },
            {
                DisplayName:'Greater Than Or Equals',
                Value: 'GreaterThanOrEquals',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAASElEQVQoU2NkQID/SGwQkxGND+aiCxLUhM0UvJqwWsvAwIBTEy4NIOdi1UQVG0jyA0mhRFAxzsjBFmEwMWRPo9uArg+slmQNAKgtCw0+5tAJAAAAAElFTkSuQmCC',
                AppliesTo: ColumnFilter.ColumnTypes.Number,
                IsDefault: true
            }
        ];



        
        
	    // Attach a listener to the document which listens for DataTables initialisation
	    // events so we can automatically initialise
	    $(document).on('preInit.dt.dtColumnFilter', function (e, settings, json) {
            if (e.namespace !== 'dt') {
	            return;
            }
            var init = settings.oInit.columnFilter;
            var defaults = dataTable.defaults.columnFilter;

            if ( init || defaults ) {
                var opts = $.extend( {}, init, defaults );

                if ( init !== false ) {
                    new ColumnFilter( settings, opts );
                }
            }
        });	        
        
        /**
            * Version information
            *
            * @name ColumnFilter.version
            * @static
        */
       ColumnFilter.version = '1.0.0';

       $1.fn.DataTable.ColumnFilter = ColumnFilter;
       $1.fn.dataTable.ColumnFilter = ColumnFilter;
    return ColumnFilter;
}));

